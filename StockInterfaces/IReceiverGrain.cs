﻿using System;
using System.Threading.Tasks;
using Orleans;

namespace StockInterfaces
{
    public interface IReceiverGrain :  IGrainWithIntegerKey
    {
        Task ActivateGrain();
    }
}
