﻿using System;
using System.Threading.Tasks;
using Intrinio.SDK.Api;
using Intrinio.SDK.Client;
using Intrinio.SDK.Model;
using Orleans;
using Orleans.Streams;
using Stocks.Interfaces;

namespace Stocks.Grains
{

    public class StockGrain : Grain, IStockGrain
    { 

        RealtimeStockPrice result = null;
        IAsyncStream<string> stream;
      

        public override async Task OnActivateAsync()
        {
            var ticker = this.GetPrimaryKeyString();

            var guid = Guid.NewGuid();
            var streamProvider = GetStreamProvider("SMSProvider");
            stream = streamProvider.GetStream<string>(guid, "StockPrice");

            await UpdatePrice(ticker);

            RegisterTimer(
                UpdatePrice,
                ticker,
                TimeSpan.FromSeconds(10),
                TimeSpan.FromSeconds(10)); 

            await base.OnActivateAsync();
        }

        async Task UpdatePrice(object stock)
        { 
            var priceTask = GetPriceQuote(stock as string); 
            await Task.WhenAll(priceTask); 
            var lastPrice = priceTask.Result;  
            await stream.OnNextAsync(lastPrice); 
        } 

        public Task<Guid> SubscribeToStockPriceAsync()
        {  
            return Task.FromResult(stream.Guid);
        }

        public async Task<string> GetPriceQuote(string identifier)
        {
           
            Configuration.Default.AddApiKey("api_key", "OmJmYmJmYThiNGY5NmY0ZTVhNjI1ZDQzMmU5ZGJkODcx");

            var securityApi = new SecurityApi(); 
            try
            {
                result = await securityApi.GetSecurityRealtimePriceAsync(identifier, null);
                 
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when calling StockExchangeApi.GetStockExchangeRealtimePrices: " + e.Message);
            }

            return result.LastPrice.ToString();
        }

        public Task<string> GetPrice()
        {
            return  Task.FromResult(result.LastPrice.ToString());
        }
    }

}