﻿using System;
using System.Threading.Tasks;
using Orleans;
using StockInterfaces;
using System.Linq;
using Orleans.Streams;

namespace StockGrains
{
    [ImplicitStreamSubscription("RANDOMDATA")]
    public class ReceiverGrain : Grain, IReceiverGrain
    {
        public override async Task OnActivateAsync()
        {

          


            await base.OnActivateAsync();
        }

        public Task ActivateGrain()
        {
            return Task.CompletedTask;
        }
    }
}
