﻿using System;
using System.Threading.Tasks;
using Orleans;
using Orleans.Streams;
using Orleans.Configuration;
using Orleans.Hosting;
using Stocks.Interfaces;

namespace StockClient
{
    class Program
    {
        static void Main(string[] args)
        { 
            var client =  InitClient().Result;
            string input;
            PrintHints();

            do
            {
                input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input)) continue;

                if (input.StartsWith("/s", StringComparison.Ordinal))
                {
                    string ticker = input.Replace("/s", "").Trim(); 

                    PrettyConsole.Line($"Subscribing to stock stream: {ticker}",ConsoleColor.Magenta);

                    SubscribeToStream(client, ticker).GetAwaiter().GetResult();
                }

                if (input.StartsWith("/u", StringComparison.Ordinal))
                {
                    string ticker = input.Replace("/u", "").Trim();
                    PrettyConsole.Line($"Unsubscribing from stock stream: {ticker}",ConsoleColor.Magenta);
                    UnsubscribeToStream(client, input.Replace("/u", "").Trim()).GetAwaiter().GetResult();
                }



            } while (input != "/exit"); 
        }

        private static async Task UnsubscribeToStream(IClusterClient client, string ticker)
        {
            var stockGrain = client.GetGrain<IStockGrain>(ticker);

            var streamIdentifier = await stockGrain.SubscribeToStockPriceAsync();

            var streamProvider = client.GetStreamProvider("SMSProvider");

            var stream = streamProvider.GetStream<string>(streamIdentifier, "StockPrice");

            var subscriptionHandles = await stream.GetAllSubscriptionHandles();
            foreach (var handle in subscriptionHandles)
            {
                await handle.UnsubscribeAsync();
            }

        }


        private static async Task SubscribeToStream(IClusterClient client, string ticker)
        {

            var stockGrain = client.GetGrain<IStockGrain>(ticker);  

            var streamIdentifier = await stockGrain.SubscribeToStockPriceAsync();

            var streamProvider = client.GetStreamProvider("SMSProvider");

            var stream = streamProvider.GetStream<string>(streamIdentifier, "StockPrice");

            await stream.SubscribeAsync(async (x, y) =>
                {
                    await Write(x);
                }
            );
        }


        static void PrintHints()
        {
            var menuColor = ConsoleColor.Magenta;
            PrettyConsole.Line("Type '/s <channel>' to subscribe to specific stocks stream", menuColor);
            PrettyConsole.Line("Type '/u <channel>' to subscribe to specific stocks stream", menuColor);
            PrettyConsole.Line("Type '/exit' to exit client.", menuColor);
        }

        private static async Task Write(string pr)
        {
           await Task.Run(() => Console.WriteLine(pr));

        }

        private static async Task<IClusterClient> InitClient()
        {

            IClusterClient client;
            client = new ClientBuilder()
                .UseLocalhostClustering()
                 .AddSimpleMessageStreamProvider("SMSProvider")
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "StockTickerApp";
                })
               // .ConfigureLogging(logging => logging.AddConsole())
             
                .Build();

            await client.Connect();
            Console.WriteLine("Client successfully connect to silo host");
            return client;
        }

        public static class PrettyConsole
        {
            public static void Line(string text, ConsoleColor colour = ConsoleColor.White)
            {
                var originalColour = Console.ForegroundColor;
                Console.ForegroundColor = colour;
                Console.WriteLine(text);
                Console.ForegroundColor = originalColour;
            }
        }
    }
}
