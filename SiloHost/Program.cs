﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Streams; 
using Stocks.Grains;
using Stocks.Interfaces;

namespace SiloHost
{
    public class Program
    { 
       
        public static int Main(string[] args)
        {
            return RunSilo().Result;
        }

        private static async Task<int> RunSilo()
        {
            try
            {
                var host = await StartSilo();
                Console.WriteLine("Press Enter to terminate..."); 

                Console.ReadLine();

                await host.StopAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
        } 

        private static async Task<ISiloHost> StartSilo()
        {
            var builder = new SiloHostBuilder()
                 .UseLocalhostClustering()

                .AddSimpleMessageStreamProvider("SMSProvider")
                .AddMemoryGrainStorage("PubSubStore")

                 .Configure<ClusterOptions>(options =>
                 {
                     options.ClusterId = "dev";
                     options.ServiceId = "StockTickerApp";
                 })
                 .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = System.Net.IPAddress.Loopback)
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(StockGrain).Assembly).WithReferences());
               //  .ConfigureLogging(logging => logging.AddConsole());

            var host = builder.Build();
            await host.StartAsync();
            return host;
        } 
    }


}


